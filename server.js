﻿require('rootpath')();
const express = require('express');
const bodyParser = require('body-parser');
const cors = require("cors");
const jwt = require('./app/_helpers/jwt');
const errorHandler = require('./app/_helpers/error-handler');
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require('swagger-ui-express');
const config = require('config.json');
const app = express();
const ENV = config.app_env;
var BASE_URL = (ENV == 'local') ? config.local_url : config.dev_url;
var SWAG_URL = (ENV == 'local') ? '192.168.1.24' : config.dev_url;
var PORT = (ENV == 'local') ? config.local_port : config.dev_port;


app.get('/', function(req, res) { res.redirect('/documentation'); });
app.use(jwt());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

global.__basedir = __dirname;

//Routes
app.use('/users', require('./app/users/users.controller'));
app.use('/admin/uploadfile', require('./app/exceldata/exceldata.controller'));
app.use('/admin/categories', require('./app/categories/categories.controller'));
app.use('/filters', require('./app/filters/filters.controller'));

app.use('/' + config.uploadDir, express.static(__dirname + '/' + config.uploadDir));
app.use(errorHandler);

//Swagger Configurations
const options = {
    definition: {
        openapi: "3.0.0",
        info: {
            title: "CEL Inventory API",
            version: "1.0.2",
            description: "CEL(CORPORATE ELECTRIC LTD) Inventory API built in Node/Express with MongoDB",
            license: {
                "name": "MIT License",
                "url": "https://opensource.org/licenses/MIT"
            }
        },
        "components": {
            securitySchemes: {
                bearerAuth: {
                    "type": "http",
                    "description": "Enter JWT Bearer Token",
                    "scheme": "bearer",
                    "bearerFormat": "JWT"
                },
            },
        },
        servers: [{
            url: 'http://' + SWAG_URL + ':' + PORT,
            description: ENV + ' server',
        }, ],
    },
    apis: ['./swagger_operations/*.js'],
};

const specs = swaggerJsdoc(options);
app.use(
    "/documentation",
    swaggerUi.serve,
    swaggerUi.setup(specs)
);



const port = PORT;
const server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});