const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    heading: { type: String, required: false, default: '' },
    options: { type: Array, required: false, default: [] },
    category_id: { type: Schema.Types.ObjectId, ref: 'Category', required: true }
});

schema.set('toJSON', { virtuals: true, versionKey: false });

module.exports = mongoose.model('Filter', schema);