﻿const express = require('express');
const router = express.Router();
const filterService = require('./filters.service');

// route
router.get('/get-categories', getAllCategories);
router.post('/get-filters', getFilterData);
router.post('/get-options', getOptionData);
router.post('/get-data', getData);
router.post('/get-search-data', getSearchData);


module.exports = router;

function getAllCategories(req, res) {
    filterService.getAllCategories()
        .then(categories => categories ? res.json({ status: true, message: 'Data found.', data: categories }) : res.json({ status: false, message: 'Data not found.', data: false }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function getFilterData(req, res) {

    filterService.getFilterData(req.body)
        .then(filters => filters && filters.length > 0 ? res.json({ status: true, message: 'Data found.', data: filters }) : res.json({ status: false, message: 'Data not found.', data: [] }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function getOptionData(req, res) {

    filterService.getOptionData(req.body)
        .then(options => options && options.length > 0 ? res.json({ status: true, message: 'Data found.', data: options }) : res.json({ status: false, message: 'Data not found.', data: [] }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function getData(req, res) {

    filterService.getData(req.body)
        .then(result => result ? res.json({ status: true, message: 'Data found.', data: result }) : res.json({ status: false, message: 'Data not found.', data: [] }))
        .catch(err => res.status(400).json({ status: false, message: err }));

}

function getSearchData(req, res) {

    filterService.getSearchData(req.body)
        .then(result => result ? res.json({ status: true, message: 'Data found.', data: result }) : res.json({ status: false, message: 'Data not found.', data: [] }))
        .catch(err => res.status(400).json({ status: false, message: err }));

}