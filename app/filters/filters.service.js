﻿const { Filter, Category, ExcelData } = require("../_helpers/db");

module.exports = {
    getAllCategories,
    getData,
    getSearchData,
    getFilterData,
    getOptionData
};


async function getAllCategories() {
        
    let result = await ExcelData.find().distinct('category');

    if (result && result.length > 0) {
        return result;
    } else {
        return false;
    }
}

async function getFilterData(params) {
    var filterArray = [];
    let category = await Category.findOne({name:params.category_name}).select('id');   

    if(category){
        let filters = await Filter.find({ category_id: category._id }).select('heading -_id');

        if(filters){

            for(var x=0; x<filters.length; x++){
				filterArray[x] = filters[x].heading;	                
            }
            return filterArray;
        }else{
            return false;
        }
    } else {
            return false;
    }
}


async function getOptionData(param) {

    if(param.category_name){
        var where = {};
		let next_column_name = getString(param.next_filter);
		
        let filters = param.filters;
    
        where['category'] =  param.category_name;    
    
        if (filters) {
            for (i = 0; i < filters.length; i++) {

                let column_name = filters[i].heading;
                let option = filters[i].option;
                where[column_name] = option;             
            }
        }
    
        let result = await ExcelData.find(where).distinct(next_column_name);               
               
        if (result && result.length > 0) {

           return result;
        }else{
            return false;
        }
    }else{
        return false;
    }
}


async function getData(param) {

    if (param.category_name) {
        var where = {};
		let filters = param.filters;
        where['category'] = param.category_name;

        if (filters) {
            for (i = 0; i < filters.length; i++) {
                let column_name = getString(filters[i].heading);				
                let option;
                option = filters[i].option;
                where[column_name] = option;
            }
        }
        const query = ExcelData.find(where);
        let result = await query.select('-_id inventory_part_number trade_description cost_usd');

        if (result && result.length > 0) {
            return result;
        } else {
            return false;
        }
    }else{
        return false;
    }

}

async function getSearchData(param) {

    const query = ExcelData.find({trade_description:{'$regex' : param.search_value, '$options' : 'i'}});
    let result = await query.select('-_id inventory_part_number trade_description cost_usd');

    if (result && result.length > 0) {
        return result;
    } else {
        return false;
    }   
}

function getString(str){
	
	if(str !== ''){
		str = str.toLowerCase(); //to lowercase
		str = str.split(" ").join("_"); //remove space and add underscore
		str = str.split("/").join("_"); //remove slash and add underscore	
	}
	return str;
}