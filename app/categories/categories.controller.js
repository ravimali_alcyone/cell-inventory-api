﻿const express = require('express');
const router = express.Router();
const categoryService = require('./categories.service');

// route
router.post('/add', add);
router.post('/cleardata', cleardata);
router.get('/', getAll);
router.get('/:id', getById);
router.put('/edit/:id', update);
router.delete('/:id', _delete);

module.exports = router;

function add(req, res) {
    categoryService.create(req.body)
        .then(category => category ? res.status(201).json({ status: true, message: 'Category added successfully.', data: category }) : res.status(400).json({ status: false, message: 'Bad request' }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function getAll(req, res) {
    categoryService.getAll()
        .then(categories => res.json({ status: true, message: 'Data found.', data: categories }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function getById(req, res) {
    categoryService.getById(req.params.id)
        .then(category => category ? res.json({ status: true, message: 'Data found.', data: category }) : res.status(404).json({ status: false, message: 'Data not found.' }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function update(req, res) {
    categoryService.update(req.params.id, req.body)
        .then(category => category ? res.json({ status: true, message: "Category updated successfully", data: category }) : res.status(404).json({ status: false, message: 'Data not found.' }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function _delete(req, res) {
    categoryService.delete(req.params.id)
        .then(category => category ? res.json({ status: true, message: "Category deleted successfully" }) : res.status(404).json({ status: false, message: 'Data not found.' }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}

function cleardata(req, res) {
    categoryService.cleardata()
        .then(result => result ? res.status(200).json({ status: true, message: 'Data cleared successfully.'}) : res.status(400).json({ status: false, message: 'Bad request' }))
        .catch(err => res.status(400).json({ status: false, message: err }));
}