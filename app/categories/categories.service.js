﻿const { Filter, Category, ExcelData } = require("../_helpers/db");

module.exports = {
    getAll,
    getById,
    create,
    delete: _delete,
    update,
    cleardata,
};

async function create(param) {

    if (await Category.findOne({ name: param.name.trim() })) {
        throw 'Name "' + param.name.trim() + '" is already taken';
    }

    const category = new Category({
        name: param.name.trim(),
        isActive: true
    });

    const data = await category.save();
    if (data) {
        //Filters
        if (param.filters) {
            for (item of param.filters) {
                let filter = new Filter({
                    heading: item.heading,
                    options: item.options,
                    category_id: data.id
                });

                filter.save();
                data.filters.push(filter.id);
                //myFilters.push(filter.id);
            }

            await data.save();

            let res = await Filter.find({ '_id': { $in: data.filters } }).select();

            if (res) {
                data.filters = res;
            }
            return data;
        } else {
            return data;
        }

    } else {
        return false;
    }
}

async function getAll() {
    return await Category.find().select().populate("filters", "heading options id category_id").sort({ name: 'asc' });
}

async function getById(id) {
    return category = await Category.findById(id).select().populate("filters", "heading options id category_id");
}

async function update(id, param) {
    const category = await Category.findById(id);

    if (!category) {
        return false;
    } else {

        if (category.name !== param.name.trim() && await Category.findOne({ name: param.name.trim() })) {
            throw 'Name "' + param.name.trim() + '" is already taken';
        }

        //Filters
        var myFilters = [];

        if (param.filters) {

            for (item of param.filters) {

                if (item.id == '') {

                    let filter = new Filter({
                        heading: item.heading,
                        options: item.options,
                        category_id: category.id
                    });

                    await filter.save();

                    myFilters.push(filter.id);

                } else {
                    let filter = await Filter.findById(item.id);

                    if (filter) {
                        let f_input = {
                            heading: item.heading,
                            options: item.options,
                            category_id: category.id
                        };

                        Object.assign(filter, f_input);

                        await filter.save();
                        myFilters.push(item.id);
                    }
                }
            }
        }

        const input = {
            name: param.name,
            filters: myFilters,
            updatedBy: param.user_id,
            updatedAt: Date.now()
        };

        Object.assign(category, input);

        if (await category.save()) {
            return getById(id);
        }
    }
}

async function _delete(id) {
    const category = Category.findById(id);
    if (!category) return false;
    Filter.findOneAndDelete({ category_id: { $gte: id } });
    return await Category.findByIdAndRemove(id);
}

async function cleardata() {
    try {
        await Category.deleteMany();
        await Filter.deleteMany();
        await ExcelData.deleteMany();

        return true;

    } catch (err) {
        return false;
    }

}