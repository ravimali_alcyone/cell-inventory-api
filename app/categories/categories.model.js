const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    name: { type: String, required: false, default: '' },
    filters: [{ type: Schema.Types.ObjectId, ref: 'Filter' }],
    isActive: { type: Boolean, required: false, default: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: false },
    updatedBy: { type: Schema.Types.ObjectId, ref: 'User', required: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true, versionKey: false });

module.exports = mongoose.model('Category', schema);