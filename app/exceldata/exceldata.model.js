const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
		category_id: { type: Schema.Types.ObjectId, ref: 'Category', required: false, default: '' },
		accubid_number: { type: String, required: false, default: '' },
		description: { type: String, required: false, default: '' },
		quantity_per_cost: { type: String, required: false, default: '' },
		price_code: { type: String, required: false, default: '' },
		trade_description: { type: String, required: false, default: '' },
		manufacturer_name: { type: String, required: false, default: '' },
		catalog_number: { type: String, required: false, default: '' },
		location: { type: String, required: false, default: '' },
		cost_usd: { type: String, required: false, default: '' },
		kyd_sell_price: { type: String, required: false, default: '' },
		inventory_part_number: { type: String, required: false, default: '' },
		category: { type: String, required: false, default: '' },
		type: { type: String, required: false, default: '' },
		size: { type: String, required: false, default: '' },
		colour: { type: String, required: false, default: '' },
		voltage: { type: String, required: false, default: '' },
		cable: { type: String, required: false, default: '' },
		brand: { type: String, required: false, default: '' },
		style: { type: String, required: false, default: '' },
		pole: { type: String, required: false, default: '' },
		device: { type: String, required: false, default: '' },
		length: { type: String, required: false, default: '' },
		fixture: { type: String, required: false, default: '' },
	},
	{ strict: false }
);

schema.set('toJSON', { virtuals: true, versionKey: false });

module.exports = mongoose.model('Excel', schema);