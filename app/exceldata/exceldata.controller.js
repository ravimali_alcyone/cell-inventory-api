﻿const express = require('express');
const multer = require('multer');
const fs = require('fs');
const excelToJson = require('convert-excel-to-json');
const XLSX = require('xlsx');
const { Filter, Category, ExcelData } = require("../_helpers/db");
const router = express.Router();

// Multer Upload Storage
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, __basedir + '/uploads/')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
    }
});

const upload = multer({ storage: storage });

// route
router.post('/', upload.single("uploadfile"), uploadFile);
module.exports = router;

function uploadFile(req, res) {
    if (!req.file.originalname.match(/\.(xls|xlsx)$/)) {
        res.status(400).json({
            status: false,
            message: 'Please upload a excel file.',
        });
    }

    let workbook = {};
    let sourceFile = __basedir + '/uploads/' + req.file.filename;

    workbook = XLSX.readFile(sourceFile, {
        sheetStubs: true,
        cellDates: true
    });

    if (workbook && workbook.Sheets) {
        let sheets = Object.keys(workbook.Sheets);

        importExcelData2MongoDB(sourceFile, sheets);
        res.status(200).json({
            status: true,
            message: 'File imported successfully.'
        });
    } else {
        res.status(400).json({
            status: false,
            message: 'This file does not contain any sheet.'
        });
    }
}

// Import Excel File to MongoDB database
async function importExcelData2MongoDB(filePath, sheetNames) {
    // Read Excel File to Json Data
    let sheetData = [];

    if (sheetNames) {
        for (let i = 0; i < sheetNames.length; i++) {
            let sheet_real_name = sheetNames[i];
            let sheet_name = sheetNames[i].trim(); // trim remove spaces

            if (sheet_real_name == 'NetPricer - Extract from BidWin' || sheet_real_name == 'KEY' || sheet_real_name == 'SEARCH FILTERS' || sheet_real_name == 'FULL MASTER') {
                if (sheet_real_name == 'SEARCH FILTERS') {
                    let excelData = excelToJson({
                        sourceFile: filePath,
                        header: {
                            rows: 1
                        },
                        sheets: ['SEARCH FILTERS'],
                        columnToKey: {
                            '*': '{{columnHeader}}'
                        }
                    });

                    // Get total sheets name
                    let totalCategories = [];
                    excelData['SEARCH FILTERS'].filter(function (data) {
                        if (totalCategories.includes(data.Category)) {
                            return false;
                        }
                        totalCategories.push(data.Category);
                        return true;
                    });

                    // For category
                    let categoryData = [];
                    for (let index = 0; index < totalCategories.length; index++) {
                        let categoryName = totalCategories[index];
                        categoryData.push({ name: categoryName, filters: [] });
                    }

                    // For filters
                    let filters = [];
                    excelData['SEARCH FILTERS'].filter(function (data) {
                        let filter = { Category: data.Category, Filter: data.Filter };
                        filters.push(filter);
                        return true;
                    });
                    filters = filters.filter(function (a) {
                        var key = a.Category + '|' + a.Filter;
                        if (!this[key]) {
                            this[key] = true;
                            return true;
                        }
                    }, Object.create(null));
                    for (let index = 0; index < filters.length; index++) {
                        let filter = filters[index];
                        for (let index2 = 0; index2 < categoryData.length; index2++) {
                            const category = categoryData[index2];
                            if (category.name == filter.Category) {
                                categoryData[index2].filters.push({ id: '', heading: filter.Filter, options: [] });
                            }
                        }
                    }

                    // For options
                    excelData['SEARCH FILTERS'].filter(function (data) {
                        for (let index = 0; index < categoryData.length; index++) {
                            let category = categoryData[index];
                            for (let index2 = 0; index2 < category.filters.length; index2++) {
                                let filter = category.filters[index2];
                                if (data.Category == category.name && data.Filter == filter.heading) {
                                    category.filters[index2].options.push(data.Option);
                                }
                            }
                        }
                        return true;
                    });

                    for (let j = 0; j < categoryData.length; j++) {
                        createCategory(categoryData[j]);
                    }
                }
            } else {
                let tbl = sheetNames[i].replace(/[^a-zA-Z ]/g, ""); //remove special charaters
                tbl = tbl.split(" ").join(""); //remove space
                tbl = tbl.toLowerCase(); //to lowercase
                let columnData = getColumnData(filePath, sheet_name);

                let sh = {
                    name: sheet_real_name, // Excel Sheet Name
                    table: tbl,
                    header: { rows: 1 }, // Header Row -> be skipped and will not be present at our result object.
                    columnToKey: columnData // Mapping columns to keys
                };
                sheetData.push(sh);
            }
        }
    }

    if (sheetData) {
        const excelData = excelToJson({
            sourceFile: filePath,
            sheets: sheetData
        });

        for (let x = 0; x < sheetData.length; x++) {
            let categoryName = sheetData[x].name;
            let tableData = excelData[categoryName];

            if (tableData) {
                for (y = 0; y < tableData.length; y++) {

                    if (tableData[y].inventory_part_number !== '') {
                        let whereCondition = { inventory_part_number: tableData[y].inventory_part_number, category: categoryName };
                        let setData = { $set: tableData[y] };
                        let options = { upsert: true, new: true };

                        try {
                            await ExcelData.updateOne(whereCondition, setData, options);
                        } catch (err) {
                            console.log('Error', err);
                        }  
                    }
                }
            } 
        }
        fs.unlinkSync(filePath);
    }
}

function getColumnData(filePath, table) {
    let result = {};
    let alphabets = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',]
    let headings = [];
    let data = excelToJson({
        sourceFile: filePath,
        sheets: [table],
        columnToKey: {
            '*': '{{columnHeader}}'
        }
    });
    data = data[table];

    if (data[0] != null && data[0] != undefined) {
        let columns = Object.keys(data[0]);
        for (let x = 0; x < columns.length; x++) {
            let column = columns[x];
            column = column.toLowerCase();
            column = column.split(" ").join("_");
            column = column.split("/").join("_");
            headings.push(column);
        }

        for (let i = 0; i < headings.length; i++) {
            let heading = headings[i];
            result[alphabets[i]] = heading;
        }
    }
    return result;
}

async function createCategory(param) {
    let checkCat = await Category.findOne({ name: param.name.trim() });
    var data;
    if (checkCat) {
        data = checkCat;
    } else {
        let category = new Category({
            name: param.name.trim(),
            isActive: true
        });
        data = await category.save();
    }

    if (data) {
        if (param.filters) {
            for (var f = 0; f < param.filters.length; f++) {
                var item = param.filters[f];
                let checkFilter = await Filter.findOne({ heading: item.heading, category_id: data.id });
                var filter;
				
                if (checkFilter) {
                    filter = checkFilter;
                    Object.assign(filter, { options: item.options });
                    await filter.save();
                } else {
                    filter = new Filter({
                        heading: item.heading,
                        options: item.options,
                        category_id: data.id
                    });
                    filter.save();
                }
                data.filters.push(filter.id);
            }

            await data.save();
            let res = await Filter.find({ '_id': { $in: data.filters } }).select();

            if (res) {
                data.filters = res;
            }
            return data;
        } else {
            return data;
        }
    } else {
        return false;
    }
}