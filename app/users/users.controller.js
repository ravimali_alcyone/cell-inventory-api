﻿const { array } = require('@hapi/joi');
const express = require('express');
const router = express.Router();
const userService = require('./users.service');
const { registerValidation, loginValidation } = require('./users.validation');

// routes
router.post('/authenticate', loginValidation, authenticate);
router.post('/register', registerValidation, register);
router.post('/forgot-password', forgotpass);
router.post('/reset-password', resetpass);
router.get('/', getAll);
router.get('/:id', getById);
router.put('/changepass/:id', changePass);
router.put('/status-update/:id', statusUpdate);
router.delete('/:id', _delete);

module.exports = router;

function register(req, res, next) {
    userService.create(req.body)
        .then(user => user ? res.status(201).json({ status: true, message: 'Registered successfully.', data: user }) : res.status(400).json({ status: false, message: 'Bad request' }))
        .catch(err => next(res.status(400).json({ status: false, message: err })));
}

function authenticate(req, res, next) {

    userService.authenticate(req.body)
        .then(user => user ? (user && user.isActive == true ? res.json({ status: true, message: 'Login confirmed.', data: user }) : res.json({ status: false, message: 'User not activated.' })) : res.json({ status: false, message: 'Invalid credentials.' }))
        .catch(err => next(err));
}

function forgotpass(req, res, next) {
    userService.forgotPass(req.body)
        .then(user => user ? res.status(200).json({ status: true, message: 'OTP sent successfully, please check your email inbox.' }) : res.status(400).json({ status: false, message: 'Email is not registered with us.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function resetpass(req, res, next) {
    userService.resetPass(req.body)
        .then(user => user ? res.status(200).json({ status: true, message: 'Password reset successfully.' }) : res.status(400).json({ status: false, message: 'OTP is not valid.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function getAll(req, res, next) {
    userService.getAll()
        .then(users => res.json({ status: true, message: 'Data found.', data: users }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function getById(req, res, next) {
    userService.getById(req.params.id)
        .then(user => user ? res.json({ status: true, message: 'Data found.', data: user }) : res.status(400).json({ status: false, message: 'User not found.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function changePass(req, res, next) {
    userService.changePass(req.params.id, req.body)
        .then(user => user ? res.json({ status: true, message: "Password updated successfully" }) : res.status(400).json({ status: false, message: 'User not found.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function statusUpdate(req, res, next) {

    userService.statusUpdate(req.params.id, req.body)
        .then(user => user ? res.json({ status: true, message: "Status updated successfully", data: user }) : res.status(400).json({ status: false, message: 'User not found.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}

function _delete(req, res, next) {
    userService.delete(req.params.id)
        .then(user => user ? res.json({ status: true, message: "User Deleted successfully" }) : res.status(400).json({ status: false, message: 'User not found.' }))
        .catch(err => next(res.json({ status: false, message: err })));
}