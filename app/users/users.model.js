const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const schema = new Schema({
    fullName: { type: String, required: false, default: '' },
    email: { type: String, unique: true, required: true },
    password: { type: String, required: false, default: '' },
    phone: { type: String, required: false, default: '' },
    isAdmin: { type: Boolean, required: false, default: false },
    isActive: { type: Boolean, required: false, default: false },
    otp_code: { type: String, required: false, default: '' },
    otp_valid_at: { type: Date, required: false, default: null },
    is_pass_req: { type: Boolean, required: false, default: false },
    createdDate: { type: Date, default: Date.now }
});

schema.set('toJSON', { virtuals: true, versionKey: false });

module.exports = mongoose.model('User', schema);