﻿const config = require("config.json");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
var nodemailer = require("nodemailer");
const { User } = require("../_helpers/db");

module.exports = {
    authenticate,
    getAll,
    getById,
    create,
    delete: _delete,
    changePass,
    forgotPass,
    resetPass,
    statusUpdate,
};

async function create(userParam) {
    if (await User.findOne({ email: userParam.email })) {
        throw 'email "' + userParam.email + '" is already taken';
    }

    const user = new User({
        fullName: userParam.fullName,
        password: bcrypt.hashSync(userParam.password, 10),
        email: userParam.email,
        phone: userParam.phone,
        isAdmin: false,
        isActive: true
    });

    //Email send functionality need to be implemented.
    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        port: config.mail_port, // true for 465, false for other ports
        host: config.mail_host,
        auth: {
            user: config.mail_auth_user,
            pass: config.mail_auth_pass,
        },
        tls: {
            rejectUnauthorized: false
        },
        secure: config.mail_is_secure,
    });

    const mailOptions = {
        from: config.mail_from_email, // sender address
        to: user.email, // list of receivers
        subject: 'Welcome Email - CEL',
        text: 'Welcome Email',
        html: 'Dear <b>' + user.fullName + '</b>,<br/> You are successfully registered.<br/> ',
    };

    transporter.sendMail(mailOptions, function(err, info) {
        if (err) {
            console.log('*** Error', err);
        } else {
            console.log('*** Success', info);
        }

    });

    const data = await user.save();
    if (data) {

        let res = await User.findById(data.id).select("-password -createdDate");

        if (res) {
            return res;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

async function authenticate({ email, password }) {
    const user = await User.findOne({ email });
    if (user && bcrypt.compareSync(password, user.password)) {
        const { password, otp_code, otp_valid_at, is_pass_req, __v, createdDate, ...userWithoutHash } = user.toObject();
        const token = jwt.sign({ id: user.id }, config.secret, {
            expiresIn: "9h"

        });

        var expTime = new Date();
        expTime.setHours(expTime.getHours() + 9); //9 hours token expiration time
        //expTime.setMinutes(expTime.getMinutes() + 2);
        expTime = expTime.getTime();
        return {
            ...userWithoutHash,
            token,
            expTime
        };

    }
}

async function forgotPass(userParam) {
    const email = userParam.email;
    const user = await User.findOne({ email });

    if (user) {

        var otp_code = Math.floor(1000 + Math.random() * 9000);
        var otpExpTime = new Date();
        otpExpTime.setMinutes(otpExpTime.getMinutes() + parseInt(config.otp_min)); // 5 minutes
        var otp_valid_at = otpExpTime;
        var is_pass_req = true;

        const input = {
            "otp_valid_at": otp_valid_at,
            "otp_code": otp_code,
            "is_pass_req": is_pass_req,
        };

        Object.assign(user, input);

        //Email send functionality need to be implemented.
        // create reusable transporter object using the default SMTP transport
        const transporter = nodemailer.createTransport({
            port: config.mail_port, // true for 465, false for other ports
            host: config.mail_host,
            auth: {
                user: config.mail_auth_user,
                pass: config.mail_auth_pass,
            },
            tls: {
                rejectUnauthorized: false
            },
            secure: config.mail_is_secure,
        });

        const mailOptions = {
            from: config.mail_from_email, // sender address
            to: user.email, // list of receivers
            subject: 'OTP generated for reset password.',
            text: 'OTP',
            html: 'Dear <b>' + user.fullName + '</b>,<br/> password reset OTP is - <b>' + otp_code + ' </b><br/> It will expire in ' + config.otp_min + ' minutes.',
        };

        // var is_mail_send = false;

        transporter.sendMail(mailOptions, function(err, info) {
            if (err) {
                // is_mail_send = false;
                console.log('*** Error', err);
            } else {
                //  is_mail_send = true;
                console.log('*** Success', info);

            }

        });
        const data = await user.save();

        if (data) {
            return true;
        } else {
            return false;
        }

    } else {
        return false;
    }
}

async function resetPass(userParam) {
    const email = userParam.email;
    const user = await User.findOne({ email });
    const now = new Date();

    if (user && user.otp_code == userParam.otp_code && user.otp_valid_at >= now) {
        var otp_code = '';
        var otp_valid_at = null;
        var is_pass_req = false;
        var password = bcrypt.hashSync(userParam.password, 10);

        const input = {
            "otp_valid_at": otp_valid_at,
            "otp_code": otp_code,
            "is_pass_req": is_pass_req,
            "password": password,
        };

        Object.assign(user, input);

        const data = await user.save();

        if (data) {
            return true;
        } else {
            return false;
        }

    } else {
        return false;
    }
}

async function getAll() {
    return await User.find({ isAdmin: false }).select("-password -otp_code -otp_valid_at -is_pass_req -isAdmin").sort({ createdDate: 'desc' });
}

async function getById(id) {
    const user = await User.findById(id).select("-password -createdDate");
    if (!user) return false;
    return user;
}

async function changePass(id, userParam) {
    if (!userParam) throw "Invalid inputs";

    const user = await User.findById(id);

    if (!user) {
        return false;
    } else {
        if (bcrypt.compareSync(userParam.password, user.password)) {
            if (userParam.newpass === userParam.confnewpass) {
                userParam.password = bcrypt.hashSync(userParam.newpass, 10);
            } else {
                throw "Confirm password not matched.";
            }
            Object.assign(user, userParam);
            await user.save();
            return user;
        } else {
            throw "Old password not matched";
        }
    }
}

async function statusUpdate(id, userParam) {

    const user = await User.findById(id);

    if (!user) {
        return false;
    } else {
        const input = {
            "isActive": userParam.is_active
        };

        Object.assign(user, input);

        if (await user.save()) {
            return await User.findById(id).select("-password -otp_code -otp_valid_at -is_pass_req -createdDate -isAdmin");
        }
    }
}

async function _delete(id) {
    const user = User.findById(id);
    if (!user) return false;
    return await User.findByIdAndRemove(id);
}