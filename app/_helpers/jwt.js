const expressJwt = require('express-jwt');
const config = require('config.json');
const userService = require('../users/users.service');

module.exports = jwt;

function jwt() {
    const secret = config.secret;
    return expressJwt({ secret, isRevoked, algorithms: ['HS256'] }).unless({
        path: [
            /\/documentation*/,
            '/users/authenticate',
            '/users/register',
            '/users/forgot-password',
            '/users/reset-password',
            // /\/uploads*/,
        ]
    });
}

async function isRevoked(req, payload, done) {
    const url = req.originalUrl;
    const user = await userService.getById(payload.id);
    if (!user) {
        return done(null, true);
    }
    done();


};