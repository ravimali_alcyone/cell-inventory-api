// swagger_operations/excelfile.js


/**
 * @swagger
 * /admin/uploadfile:
 *   post:
 *     security:
 *      - bearerAuth: [] 
 *     summary: Upload Excel(Xlsx) File(Admin Mode)
 *     tags: [ExcelFile] 
 *     requestBody:
 *       required: true
 *       content:
 *         multipart/form-data:
 *           schema:
 *             type: object
 *             properties:
 *               uploadfile:
 *                 type: file
 *                 description: Only Excel file
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});



/**
 * @swagger
 * /admin/categories/cleardata:
 *   post:
 *     security:
 *      - bearerAuth: [] 
 *     summary: Clear Data (Admin Mode)
 *     tags: [ExcelFile] 
 *     description: Clear Data
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
 router.post('/', function(req, res) {
    // ...
});