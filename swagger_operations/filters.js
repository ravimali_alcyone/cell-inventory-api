// swagger_operations/filters.js



/**
 * @swagger
 * /filters/get-categories:
 *   get:
 *     security:
 *      - bearerAuth: []
 *     summary: Retrieve a list of categories (User/Admin Mode)
 *     tags: [Filters] 
 *     description: Retrieve a list of categories from Database.
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.get('/', function(req, res) {
    //...
});



/**
 * @swagger
 * /filters/get-filters:
 *   post:
 *     security:
 *      - bearerAuth: []
 *     summary: Get Filters By Category Name (User/Admin Mode)
 *     tags: [Filters] 
 *     description: Get Filters By Category Name 
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category_name:
 *                 type: string
 *                 description: Category Name
 *                 example: Test
 *     responses:
 *       200:
 *         description: Get Filters.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */

router.post('/', function(req, res) {
    //...
});



/**
 * @swagger
 * /filters/get-options:
 *   post:
 *     security:
 *      - bearerAuth: []
 *     summary: Get Options by Category,Filters and Next Filter (User/Admin Mode)
 *     tags: [Filters] 
 *     description: Get Options by Category,Filters and Next Filter 
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category_name:
 *                 type: string
 *                 description: Category Name
 *                 example: Test
 *               filters:
 *                 type: array
 *                 description: Filters Array
 *                 example: [] 
 *               next_filter:
 *                 type: string
 *                 description: Next Filter Name
 *                 example: Test
 *     responses:
 *       200:
 *         description: Get Filters.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */

 router.post('/', function(req, res) {
    //...
});


/**
 * @swagger
 * /filters/get-data:
 *   post:
 *     security:
 *      - bearerAuth: [] 
 *     summary: Get final Data by category, filters and options (User/Admin Mode)
 *     tags: [Filters] 
 *     description: Get final Data by category, filters and options
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               category_name:
 *                 type: string
 *                 description: Category Name
 *                 example: Test
 *               filters:
 *                 type: array
 *                 description: Filters Array
 *                 example: []
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});

/**
 * @swagger
 * /filters/get-search-data:
 *   post:
 *     security:
 *      - bearerAuth: [] 
 *     summary: Get Search Data (User/Admin Mode)
 *     tags: [Filters] 
 *     description: Get Search Data
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               search_value:
 *                 type: string
 *                 description: Search Value
 *                 example: Test
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
 router.post('/', function(req, res) {
    // ...
});

