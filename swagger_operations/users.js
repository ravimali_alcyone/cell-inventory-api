// swagger_operations/users.js

/**
 * @swagger
 * /users/register:
 *   post:
 *     summary: User Register.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               fullName:
 *                 type: string
 *                 description: The user's full name
 *                 example: John Doe
 *               phone:
 *                 type: string
 *                 description: The user's phone no.
 *                 example: 9809801234
 *               email:
 *                 type: string
 *                 description: The user's email
 *                 example: example@mail.com
 *               password:
 *                 type: string
 *                 description: The user's password
 *                 example: 123456
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});


/**
 * @swagger
 * /users/authenticate:
 *   post:
 *     summary: User Login.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: example@mail.com
 *               password:
 *                 type: string
 *                 description: The user's password.
 *                 example: 123456 
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});


/**
 * @swagger
 * /users/forgot-password:
 *   post:
 *     summary: Forgot Password - User will enter his/her email then an OTP will be sent to this email.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: example@mail.com
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});



/**
 * @swagger
 * /users/reset-password:
 *   post:
 *     summary: Reset Password - User will Enter valid OTP and new password.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *                 description: The user's email.
 *                 example: example@mail.com
 *               otp_code:
 *                 type: string
 *                 description: The OTP code.
 *                 example: 1234
 *               password:
 *                 type: string
 *                 description: The new Password.
 *                 example: 123456
 *  
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.post('/', function(req, res) {
    // ...
});


/**
 * @swagger
 * /users:
 *   get:
 *     security:
 *      - bearerAuth: []
 *     summary: Retrieve a list of users (Admin Mode)
 *     tags: [Users]
 *     description: Retrieve a list of users from Database. Can be used to populate a list of fake users when prototyping or testing an API.
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.get('/', function(req, res) {
    //...
});

/**
 * @swagger
 * /users/status-update/{id}:
 *   put:
 *     security:
 *      - bearerAuth: []
 *     summary: User Status Active/Deactive Update (Admin Mode)
 *     tags: [Users]
 *     description: Update user status using his/her id.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string 
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               is_active:
 *                 type: boolean
 *                 description: true/false.
 *                 example: true
 *     responses:
 *       200:
 *         description: Test
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */
router.put('/', function(req, res) {
    // ...
});



/**
 * @swagger
 * /users/{id}:
 *   get:
 *     security:
 *      - bearerAuth: []
 *     summary: Retrieve a single user (Admin Mode)
 *     tags: [Users]
 *     description: Retrieve a single user. Can be used to populate a user profile when prototyping or testing an API.
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user to retrieve.
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Get single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 */

router.get('/:id', function(req, res) {
    //...
});